package Pets;

public abstract class Pet implements AllPets{

    private String name;
    private int age;
    private String voice;

    public String toString() {
        return "pet info - " + name +age;
    }

    public Pet (){
        this.name=this.getName();
    }
    public Pet(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getVoice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

}
