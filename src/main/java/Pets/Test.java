package Pets;

/**
 * Created by sovaj on 18.02.2018.
 */
public class Test {
    public static void main(String[] args) {
        Dog dog = new Dog("Makoto",1);
        System.out.println(dog);
        dog.setName("Makoto");
        System.out.println("Dog's name: " + dog.getName());
        dog.setAge(1);
        System.out.println("Dog's age: " + dog.getAge());
        dog.setVoice("Гав-гав");
        System.out.println("Голос: " + dog.getVoice());

        Cat cat = new Cat("Mirta", 4);
        System.out.println(cat);
        cat.setName("Mirta");
        System.out.println("cat's name: " + cat.getName());
        cat.setAge(4);
        System.out.println("cat's age: " + cat.getAge());
        cat.setVoice("Мяу");
        System.out.println("Голос: " + cat.getVoice());


    }
}
