package lesson7;

import java.util.Random;

/**
 * Created by IT school on 14.02.2018.
 */
public class LITSUser implements iUser{

    private String name;
    private String password;
    private int ID;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void registration(String name, String password) {
        System.out.println("LITS User was registered");
    }

    public int generateID() {
        return new Random().nextInt(SECRET);
    }
}
