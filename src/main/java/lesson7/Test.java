package lesson7;

/**
 * Created by IT school on 14.02.2018.
 */
public class Test {
    public static void main(String[] args) {
        iUser iUser = new LITSUser();
        iUser.registration("name", "pass");

        iUser iUser2 = new AutoClass();
        iUser2.registration("name", "pass");

        iUser iUser3 = new SuperAutoUser();
        iUser3.registration("da", "pass");
    }
}
