package lesson7;

/**
 * Created by IT school on 14.02.2018.
 */
public interface iUser {
    int SECRET = 10;

    public void registration (String name, String password);
    public int generateID();
    
}
